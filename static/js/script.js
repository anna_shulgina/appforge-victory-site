$(document).ready(function() {
	// for forms placeholder
	function hasPlaceholderSupport() {
		var input = document.createElement('input');
		return ('placeholder' in input);
	}
	if(!hasPlaceholderSupport()){
		$('input[type=text]').each(function(){
			var message = $(this).attr('placeholder');
			if(message == undefined || message == null || message == "") return;
			$(this).focus(function(){
				var message = $(this).attr('placeholder');
				if(message == undefined || message == null || message == "") return;
				if($(this).val() == message) {
					$(this).val("").addClass("active"); //Color customization
				}
			}).blur(function(){
				var message = $(this).attr('placeholder');
				if(message == undefined || message == null || message == "") return;
				if($(this).val() == "") {
					$(this).val(message).removeClass("active"); //Color customization
				}
			}).val(message);
		});
	} 

	// login popup
	$(function(){
	    $(document).click(function(event) {
	        if ($(event.target).closest('.btn_login').length) {
	        	$('.title_switcher a:first-child').addClass('active');
	        	$('.title_switcher a:last-child').removeClass('active');
	        	$('#login_form').addClass('active');
	        	$('#registration_form').removeClass('active');

	            $('#mask').fadeToggle(500);
           		event.preventDefault();
	        } else 
	        if ($(event.target).closest('.btn_reg').length) {
	        	$('.title_switcher a:first-child').removeClass('active');
	        	$('.title_switcher a:last-child').addClass('active');
	        	$('#login_form').removeClass('active');
	        	$('#registration_form').addClass('active');

	        	$('#mask').fadeToggle(500);
           		event.preventDefault();
	        } else
	        if ($(event.target).closest('.btn_close').length) {
	        	event.preventDefault();
	            $("#mask").fadeOut(500);
	        } else
	        if ($(event.target).closest('#popup_login').length === 0) {
	            $("#mask").fadeOut(500);
	        }
	    });
	});

	// popup scrollbar
	(function($){
		$(window).load(function(){
			$(".scroll-block").mCustomScrollbar({
				scrollButtons:{
					enable:true
				}
			});
			$("#popular_games").mCustomScrollbar({
				scrollButtons:{
					horizontalScroll:true
				},
				horizontalScroll:true,
				advanced:{autoExpandHorizontalScroll:true,updateOnContentResize:false}	
			});
		});
	})(jQuery);

	// games
	elemCount = $('.all_games_list .item_pic').length;

	$(document).on("mouseenter", ".item_pic", function(){
		var curElem = $(this);
		curElem.find(".start_game").fadeIn(300);
		curElem.find(".mask").fadeOut();
		var curElemId = curElem.attr('id');
		for (var i = 0; i < elemCount; i++) {
			elem = $('.all_games_list .item_pic').eq(i);
			if(curElemId != elem.attr('id')) {
				elem.find(".mask").fadeIn(300);
			}
		}
	});

	$(document).on("mouseleave", ".item_pic", function(){
		var curElem = $(this);
		curElem.find(".start_game").fadeOut(100);
		var curElemId = curElem.attr('id');
		for (var i = 0; i < elemCount; i++) {
			elem = $('.all_games_list .item_pic').eq(i);
			if(curElemId != elem.attr('id')) {
				elem.find(".mask").fadeOut(300);
			}
		}
	});

	// game
	$(document).on('click', '.help', function(){
		$('.help_information').slideToggle(500);
		return false;
	});

	// btn
	$(document).on('click', '.disable', function(){
		return false;
	});

	// login and registration
	$(document).on('click', '#rules', function(){
		$('#registration_form .button_submit').toggleClass('disable');
	});

	$(document).on('click', '.title_switcher a', function(event){
		event.preventDefault();
		if (!$(this).hasClass('active')) {
			$('.title_switcher a').each(function(){
				$(this).toggleClass('active');
			});
			$('#auth_block').toggleClass('big');
			$('.right_side .info').each(function(){
				$(this).toggleClass('active').toggleClass('noactive');
			});
			$('#auth_block form').each(function(){
				$(this).toggleClass('active');
			});
		}
	});

	// ui slider
	$('#flag_slider').slider({
		slide: function(event, ui) {
			curPosSlider = $('#flag_slider').slider("value");
			curPos = 10.42 * (-1) * (curPosSlider -1) + 'px';
			$('.popular_games_list').animate({left: curPos}, 0);
		}
	});
});

